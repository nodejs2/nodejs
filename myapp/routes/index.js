var express = require('express');
var router = express.Router();

var mysql = require('mysql');
var db = mysql.createPool({
  host : 'localhost',
  user : 'root',
  password : null,
  database : 'nodejs',
  debug : false
})

/* GET home page. */
router.get('/', function(req, res, next) {

  // db.query("select * from tbl_users where name like '%J%' ",function(err,rs){
  db.query("select * from tbl_users ",function(err,rs){
    res.render('select',{ data: rs });
  });
});

router.get('/testconnect',function(req,res,next){
  if(db != ""){
    res.send('Success');
  }else{
    res.send('Fail');
  }
});

//Insert Into Database
router.get('/form',function(req,res,next){
  res.render('form',{book:{} });
});

router.post('/form',function(req,res,next){
  db.query('insert into tbl_users SET ?',req.body, function(err,rs){
    res.redirect('/');
  })
});

//Delete From Database
router.get('/delete',function(req,res,next){
  db.query('delete from tbl_users where id = ?', req.query.id, function(err,rs){
    res.redirect('/');
  })
})

//Update
router.get('/edit',function(req, res, next){
  db.query('select * from tbl_users where id = ?', req.query.id, function (err, rs){
    res.render('form', {book: rs[0]});
  })
})

router.post('/edit', function(req,res,next){ 
  var param = [req.body, req.query.id];
  db.query('update tbl_users set ? where id = ?', param, function(err,rs){
    res.redirect('/');
  })
})

module.exports = router;
